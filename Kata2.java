public class Game {
    public String winner(String[] deckSteve, String[] deckJosh) {
        int[] rango = new int[128];
        rango['2'] = 2;
        rango['3'] = 3;
        rango['4'] = 4;
        rango['5'] = 5;
        rango['6'] = 6;
        rango['7'] = 7;
        rango['8'] = 8;
        rango['9'] = 9;
        rango['T'] = 10;
        rango['J'] = 11;
        rango['Q'] = 12;
        rango['K'] = 13;
        rango['A'] = 14;

        int puntosSteve = 0;
        int puntosJosh = 0;

        for (int i = 0; i < deckSteve.length; i++) {
            if (rango[deckSteve[i].charAt(0)] > rango[deckJosh[i].charAt(0)]) {
                puntosSteve++;
            } else if (rango[deckSteve[i].charAt(0)] < rango[deckJosh[i].charAt(0)]) {
                puntosJosh++;
            }
        }

        if (puntosSteve > puntosJosh) {
            return "Steve wins " + puntosSteve + " to " + puntosJosh;
        } else if (puntosSteve < puntosJosh) {
            return "Josh wins " + puntosJosh + " to " + puntosSteve;
        } else {
            return "Tie";
        }
    }
}